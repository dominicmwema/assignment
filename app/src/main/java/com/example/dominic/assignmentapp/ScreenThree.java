package com.example.dominic.screenapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class ScreenThree extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_three);


    }

    public void backToScreenMain(View view) {
        Intent screeTwo = new Intent(getApplicationContext(), MainScreen.class);
        startActivity(screeTwo);

    }
}

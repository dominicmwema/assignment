package com.example.dominic.screenapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ScreenTwo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_two);


    }

    public void backToScreenMain(View view) {
        Intent screeTwo = new Intent(getApplicationContext(), MainScreen.class);
        startActivity(screeTwo);

    }
}
